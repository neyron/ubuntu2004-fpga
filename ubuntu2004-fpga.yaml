#==============================================================================
# vim: softtabstop=2 shiftwidth=2 expandtab fenc=utf-8 cc=81 tw=80
#==============================================================================
#
# DESCRIPTION: Recipe to build an Ubuntu 20.04 system with AMD/Xilinx FPGA tools
#
#==============================================================================
# This recipe extends another. To look at the step involed, run:
#   kameleon dryrun ubuntu2004-fpga.yaml
# To see the variables that you can override, use the following command:
#   kameleon info ubuntu2004-fpga.yaml
---
extend: grid5000/from_grid5000_environment/base.yaml

global:
  ### Grid'5000 environment information
  ## (Uncomment and change any line if needed)

  ## Frontend to run kaenv3 on
  #grid5000_frontend: "frontend"

  ## Site used in the build
  #grid5000_site: "grenoble"

  ## Environment to build from
  grid5000_environment_import_name: "ubuntu2004-min"
  #grid5000_environment_import_user: "deploy"
  #grid5000_environment_import_version: ""
  #grid5000_environment_import_arch: "x86_64"

  ## New environment description
  #grid5000_environment_export_name: "$${kameleon_recipe_name}"
  #grid5000_environment_export_format: "tar.zst"
  grid5000_environment_export_description: "Ubuntu2004 with Xilinx FPGA software (require access to the fpgasoftware group storage)"

  ## Set where to store the environment and the assiated kadeploy URL base
  grid5000_environment_export_dir: "/srv/storage/$${group_storage}"
  grid5000_environment_export_baseurl: "local:///srv/storage/$${group_storage}"

  ## Optionaly, the environment postinstall script can be changed, e.g. to
  ## enable NFS homes, LDAP accounts, if not enabled in the imported env.
  grid5000_environment_export_postinstall_script: "g5k-postinstall --net netplan --disk-aliases --fstab nfs"

  ## Optionaly, an additional postinstall can be given, e.g. to do some custom
  ## operations. Use the following variables to set the archive name and script.
  #grid5000_environment_export_additional_postinstall_archive: "$${kameleon_recipe_name}-additional-postinstall.tar.gz"
  #grid5000_environment_export_additional_postinstall_script: "additional_postinstall.sh"
  ## The recipe will have to prepare the additional postinstall content in a
  ## directory to create in the local context and name "additional_postinstall"
  ## by default (next line to change it). The archive is created by the export.
  #grid5000_environment_export_additional_postinstall_dir: "additional_postinstall"

  ### Target machine/CPU architecture
  ## If building an environment for another architecture than x86_64, uncomment
  ## and adapt the next lines.
  ## The following works for ARM64 machines, just uncomment for such machines.
  #qemu_arch: aarch64
  #qemu_uefi: true

  ### You can add below any other global variable definition
  ## See the variables which can be overloaded, by running:
  ##   kameleon info ubuntu2004-fpga.yaml
  ## Or define any new variable you would need. e.g.:
  #my_variable: my_value

  ## Grid'5000 group storage:
  group_storage: "fpgasoftware@storage1.grenoble.grid5000.fr"

  ## Xilinx resources
  # https://www.xilinx.com/products/boards-and-kits/alveo/u200.html#gettingStarted
  xilinx_deb_xrt: "xrt_202210.2.13.466_20.04-amd64-xrt.deb"
  xilinx_deb_tarball: "xilinx-u200-gen3x16-xdma_2022.1_2022_0415_2123-all.deb.tar.gz"
  xilinx_deb_dev: "xilinx-u200-gen3x16-xdma-2-202110-1-dev_1-3514848_all.deb"
  # https://www.xilinx.com/products/acceleration-solutions/xbtest.html#u200
  xilinx_deb_test: "xilinx_u200_gen3x16_xdma_2_202110_1_6.0_20.04.zip"

bootstrap:
  ### The bootstrap section takes in charge the import of the Grid'5000
  ## environment to customize. No modification should be needed here.
  - "@base"

setup:
  ### The setup section is where to place your customization. Add all steps
  ## required by your customization.
  ## The following is given as example only, replace with your steps.
  - upgrade_system:
    - update_package_list:
      - exec_in: apt-get update
    - upgrade:
      - exec_in: apt-get upgrade -y
      - exec_in: apt-get dist-upgrade -y
  - setup_group_storage:
    - install_grid5000_ssl_certificate:
      - local2in:
        - /usr/local/share/ca-certificates/ca2019.grid5000.fr.crt
        - /usr/local/share/ca-certificates/ca2019.grid5000.fr.crt
      - exec_in: update-ca-certificates
    - install_autofs:
      - exec_in: apt-get install -y autofs ruby
    - confugure_autofs:
      - local2in:
        - /etc/auto.master.d/storage.autofs
        - /etc/auto.master.d/storage.autofs
      - local2in:
        - /etc/auto.storage
        - /etc/auto.storage
      - exec_in: chmod 755 /etc/auto.storage
  - setup_user_accounts:
    - install_ldap_service:
      - exec_in: apt-get install -y nslcd
    - setup_ldap:
      - exec_in: mkdir -p /etc/ldap/certificates/
      - local2in:
        - /etc/ldap/certificates/ca2019.grid5000.fr.cert
        - /etc/ldap/certificates/ca2019.grid5000.fr.cert
      - local2in:
        - /etc/ldap/ldap.conf 
        - /etc/ldap/ldap.conf 
      - local2in:
        - /etc/nsswitch.conf
        - /etc/nsswitch.conf
      - local2in:
        - /etc/nslcd.conf
        - /etc/nslcd.conf
  - install_xilinx_software:
    - download_packages:
      - exec_in: wget https://www.xilinx.com/bin/public/openDownload?filename=$${xilinx_deb_xrt} -O $${xilinx_deb_xrt}
      - exec_in: wget https://www.xilinx.com/bin/public/openDownload?filename=$${xilinx_deb_tarball} -O $${xilinx_deb_tarball} 
      - exec_in: tar zxvf $${xilinx_deb_tarball} && rm  $${xilinx_deb_tarball}
      - exec_in: wget https://www.xilinx.com/bin/public/openDownload?filename=$${xilinx_deb_test} -O $${xilinx_deb_test} 
      - exec_in: unzip ./$${xilinx_deb_test} && rm ./$${xilinx_deb_test}
      - local2in:
        - /srv/storage/$${group_storage}/downloads/ubuntu-20.04/$${xilinx_deb_dev}
        - ./$${xilinx_deb_dev}
    - install_packages:
      - exec_in: apt-get install -y ./*.deb
      - exec_in: rm *.deb
  - install_requirements_for_vivado:
    - install_xauth:
      - exec_in: apt-get install -y xauth
    - install_libtinfo5:
      - exec_in: apt-get install -y libtinfo5
    - install_java:
      - exec_in: apt-get install -y openjdk-11-jdk libswt-gtk-4-java libdbus-java libslf4j-java
    - add_tools:
      - exec_in: ln -s /srv/storage/$${group_storage}/tools /tools
  - install_remote_desktop:
    - install_tigervnc:
      - exec_in: apt-get install -y tigervnc-standalone-server
    - install_desktop_environment:
      - exec_in: apt-get install -y ubuntu-desktop-minimal
  - install_some_tools:
    - install_editors:
      - exec_in: apt-get install -y vim
    - install_git_tools:
      - exec_in: apt-get install -y git tig
  - set_welcome_message:
    - set_motd:
      - write_in:
        - /etc/motd
        - |
          **********************************************************************
          * Ubuntu 20.04 with AMD Xilinx FPGA tools                            *
          *                                                                    *
          * Maintained by Pierre Neyron <pierre.neyron@imag.fr>.               *
          *                                                                    *
          * - Support for Alveo U200 is installed in /opt/xilinx.              *
          * See the kameleon recipe for details about the installation.        *
          *                                                                    *
          * - Vitis/Vivado are available in /tools/Xilinx, which is stored     *
          * on the fpgasoftware@storage1.grenoble.grid5000.fr group storage.   *
          **********************************************************************

export:
  ### The export section takes in charge the export of your customized Grid'5000
  ## environment. No modification should be needed here.
  - "@base"
